<div class="formCol">
    <div class="mdSelectWrapper">
        <select class="mdSelect inputValueCheck" name="{{$name}}" id="{{$name}}" required>
            <option value="">Select {{$title}}</option>
            @foreach($data as $plan)
                <option value="{{$plan->id}}">{{$plan ? $plan->full_name : ""}}</option>
            @endforeach
        </select>
    </div>
</div>
