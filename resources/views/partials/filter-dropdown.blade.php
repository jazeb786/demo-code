<div class="formCol">
    <div class="mdSelectWrapper">
        <select class="mdSelect inputValueCheck" name="{{$name}}" id="{{$name}}" required>
            <option value="">Select {{$title}}</option>
            @foreach($data as $d)
                @if($d->$relation)
                    <option value="{{$d->id}}">{{$d->$relation->person ? $d->$relation->person->practice_name : ""}}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>
