<header id="pageHeader">
    <div class="scrollchild">
        <div class="hUpWrap">
            <!-- logoWrap -->
            <div class="logoWrap">
                <button type="button" id="mainNavCollapse" class="mainNavToggler">
                    <i class="fas fa-align-left"></i>
                </button>
                <a href="#" class="logo">
                    <img src="https://freshdentalplan.com/wp-content/uploads/2020/07/Logo-MembersyConnect.svg" class="img-responsive" alt="Membersy">
                </a>
            </div>
            <!-- mainNav -->
            <nav class="mainNav">
                @if(auth()->user()->can('role-list') || auth()->user()->hasRole('Admin'))
                    <ul class="navbarList mainNavList">
                        <li class=" {{Request::url() == route('roles.index') ? 'active' : ''}}">
                            <a href="{{route('roles.index')}}" class="navLink">
                                <i class="fas fa-home mdIcn"></i>
                                <span>Roles</span>
                            </a>
                        </li>
                        <li class=" {{Request::url() == route('admin.index') ? 'active' : ''}}">
                            <a href="{{route('admin.index')}}" class="navLink">
                                <i class="fas fa-home mdIcn"></i>
                                <span>Create Admin</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbarList mainNavList">
                        <li class="nav-item navItemHeader">
                            <span class="text-uppercase">OFFICE MANAGEMENT</span>
                        </li>
                        {{--<li class="{{Request::url() == route('partner.index') ? 'active' : ''}}">--}}
                        {{--<a href="{{route('partner.index')}}" class="navLink">--}}
                        {{--<i class="fas fa-mail-bulk mdIcn"></i>--}}
                        {{--<span>Enrollment Partner</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="{{Request::url() == route('group.index') ? 'active' : ''}}">--}}
                        {{--<a href="{{route('group.index')}}" class="navLink">--}}
                        {{--<i class="fas fa-mail-bulk mdIcn"></i>--}}
                        {{--<span>Enrollment Group</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="{{Request::url() == url('/dso') ? 'active' : ''}}">--}}
                        {{--<a href="/dso" class="navLink">--}}
                        {{--<i class="fas fa-calendar-alt mdIcn"></i>--}}
                        {{--<span>Enrollment Group</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="{{Request::url() == url('/regions') ? 'active' : ''}}">--}}
                        {{--<a href="/regions" class="navLink">--}}
                        {{--<i class="fas fa-file-signature mdIcn"></i>--}}
                        {{--<span>Enrollment Region</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li class="{{Request::url() == url('/providers') ? 'active' : ''}}">--}}
                        {{--<a href="/providers" class="navLink">--}}
                        {{--<i class="fas fa-building mdIcn"></i>--}}
                        {{--<span>Enrollment Location</span>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                    </ul>
                @endif
                <ul class="navbarList mainNavList">
                    @can('partner-list')
                        <li class="{{Request::url() == route('partner.index') ? 'active' : ''}}">
                            <a href="{{route('partner.index')}}" class="navLink">
                                <i class="fas fa-mail-bulk mdIcn"></i>
                                <span>Enrollment Partner</span>
                            </a>
                        </li>

                    @endcan
                    @can('group-list')
                        <li class="{{Request::url() == route('group.index') ? 'active' : ''}}">
                            <a href="{{route('group.index')}}" class="navLink">
                                <i class="fas fa-mail-bulk mdIcn"></i>
                                <span>Enrollment Group</span>
                            </a>
                        </li>
                    @endcan
                    @can('region-list')
                        <li class="{{Request::url() == route('region.index') ? 'active' : ''}}">
                            <a href="{{route('region.index')}}" class="navLink">
                                <i class="fas fa-mail-bulk mdIcn"></i>
                                <span>Enrollment Region</span>
                            </a>
                        </li>
                    @endcan
                    @can('location-list')
                        <li class="{{Request::url() == route('location.index') ? 'active' : ''}}">
                            <a href="{{route('location.index')}}" class="navLink">
                                <i class="fas fa-mail-bulk mdIcn"></i>
                                <span>Enrollment Location</span>
                            </a>
                        </li>
                    @endcan
                    @can('user-list')
                        <li class="{{Request::url() == route('user.index') ? 'active' : ''}}">
                            <a href="{{route('user.index')}}" class="navLink">
                                <i class="fas fa-mail-bulk mdIcn"></i>
                                <span>User Management</span>
                            </a>
                        </li>
                    @endcan
                    @can('plan-list')
                        <li class="{{Request::url() == route('plan.index') ? 'active' : ''}}">
                            <a href="{{route('plan.index')}}" class="navLink">
                                <i class="fas fa-mail-bulk mdIcn"></i>
                                <span>Plan Management</span>
                            </a>
                        </li>
                    @endcan
                    @can('member-list')
                        <li class="{{Request::url() == route('member.index') ? 'active' : ''}}">
                            <a href="{{route('member.index')}}" class="navLink">
                                <i class="fas fa-mail-bulk mdIcn"></i>
                                <span>Member Management</span>
                            </a>
                        </li>
                    @endcan
                </ul>
            </nav>
        </div>
    </div>
</header>
