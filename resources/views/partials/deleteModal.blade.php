<div class="fancyboxpopup" id="m_modal_4_{{$id}}">
    <div class="formholder">
        <div class="head taskIsSuccessHide">
            <h2 class="margin-bottom-1">Are you sure you want to delete this Record?</h2>
        </div>
        <form class="anmForm"  enctype="multipart/form-data" id="formValidationReactivateExpiredMembership">
            <div class="popupbody formBody"></div>
        </form>
        <div class="popupfooter">
            <!-- Loader -->
            <div class="loader"><div class="loaderInside"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid"><path fill="none" stroke="#38aa9e" stroke-width="8" stroke-dasharray="42.76482137044271 42.76482137044271" d="M24.3 30C11.4 30 5 43.3 5 50s6.4 20 19.3 20c19.3 0 32.1-40 51.4-40 C88.6 30 95 43.3 95 50s-6.4 20-19.3 20C56.4 70 43.6 30 24.3 30z" stroke-linecap="round"><animate attributeName="stroke-dashoffset" repeatCount="indefinite" dur="3.571428571428571s" keyTimes="0;1" values="0;256.58892822265625"></animate></path></svg></div></div>
            <button type="button" class="btn btnLight closeFancyBox taskIsSuccessShow reload" style="display: none;">Continue</button>
            <button type="button" class="btn btnLight closeFancyBox taskIsSuccessHide">Go Back</button> <span class="taskIsSuccessHide">&nbsp;</span> 
            <a href="{{ route($route, $id) }}" class="btn btnSecondary displayOverlay taskIsSuccessHide">Delete Record</a>
        </div>
    </div>
</div>