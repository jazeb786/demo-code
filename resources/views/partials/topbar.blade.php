<div class="topBar">
    @yield('breadcrumbs')
    <div class="tbActDots">
        <ul class="navbarNav">
            <li class="dropDown">
                <a href="#" class="navLink">
                    <i class="fas fa-bell mdIcn"></i>
                    <span class="badge badgeSm" id="activity_count"></span>
                </a>
                <div class="dropdownMenu dMenuLg dMenuRight">
                    <div class="dMenuHeader">
                        <span>Latest activity</span>
                    </div>
                    <div class="dMenuBody">
                        <ul class="notList" id="latest-activity"></ul>
                    </div>
                    <div class="dMenuFooter">
                        <a href="#">All activity</a>
                        <div id="clear">
                            <a href="javascript:void(0)" id="clear-count"><i class="fas fa-check"></i></a>
                        </div>
                    </div>
                </div>
            </li>

            <li class="dropDown">
                <a href="#" class="navLink">
                    {{--                    {{Auth::user()->full_name }}--}}
                    <span class="badge badgeLg">Active</span>
                    <i class="fas fa-angle-down mdIcn"></i>
                </a>
                <div class="dropdownMenu dMenuSm dMenuRight">
                    {{--<a href="{{route('edit.admin.profile',$admin['id'])}}" class="dropDownItem">--}}
                    {{--<i class="fas fa-cog mdIcn"></i> Account Details--}}
                    {{--</a>--}}
                    {{--@if($admin->role == "admin")--}}
                    {{--<a href="{{route('role.index')}}" class="dropDownItem">--}}
                    {{--<i class="fas fa-cog mdIcn"></i> Create Marketing / Support Admin--}}
                    {{--</a>--}}
                    {{--@endif--}}
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <button type="submit" class="logout">
                            <i class="fas fa-power-off mdIcn"></i> Logout
                        </button>
                    </form>
                </div>
            </li>
        </ul>
    </div>
</div>
