<!-- Footer -->
<footer id="pageFooter">
    <div class="container">
        <div class="footerWrap">
            <div class="footerTxt">
                <span class="footerText">&copy; 2020 • Membersy LLC • All Rights Reserved</span>
            </div>
            <div class="memLogo">
                <a href="http://membersy.com/"><img src="https://thrivedentalplan.com/wp-content/uploads/2020/04/Logo-Membersy-TMsymbol.png" alt="image description" style="max-width: 200px; margin: -15px 0 -20px;"></a>
            </div>
            <ul class="ftLinks">
                <li><a target="_blank" href="https://partner.membersy.com/images/providers/pdf/membersy-dashboard-tos.pdf">Terms &amp; Conditions</a></li>
                <li><a href="#">Privacy Policy</a></li>
            </ul>
        </div>
    </div>
</footer>
<!-- /Footer -->
