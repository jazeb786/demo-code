<div class="tbBreadcrumb">
    <a href="{{$route}}" class="breadcrumbItem"><i class="{{$iconClass}}"></i> Membership Dashboard</a>
    @if($text)<span class="breadcrumbItem active">{{$text}}</span>@endif
</div>
