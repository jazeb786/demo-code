@if ($paginator->hasPages())
    <div class="dataTables_paginate paging_simple mb-0">
        @if ($paginator->onFirstPage())
            <a class="paginate_button previous disabled">← Prev</a>
        @else
            <a href="{{ $paginator->previousPageUrl() }}" class="paginate_button displaySlider previous">← Prev</a>
        @endif
        @if ($paginator->hasMorePages())
            <a class="paginate_button displaySlider next" href="{{ $paginator->nextPageUrl() }}" >Next →</a>
        @else
            <a class="paginate_button next disabled">Next →</a>
        @endif
    </div>
@endif