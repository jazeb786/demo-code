@extends('layouts.app')
@section('breadcrumbs')
    @include('partials.breadcrumb',['route' => request()->fullUrl(),'text' => 'Dashboard','pageLink' => request()->fullUrl() , 'iconClass' => "fas fa-home mdIcn"])
@endsection
@section('content')
    @include('partials.alerts')
    <div> Dashboard </div>

@endsection
