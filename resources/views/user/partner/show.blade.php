@extends('layouts.app')
@section('breadcrumbs')
    @include('partials.breadcrumb',['route' => request()->fullUrl(),'text' => 'Admin','pageLink' => request()->fullUrl() , 'iconClass' => "fas fa-home mdIcn"])
@endsection
@section('content')
    @include('partials.alerts')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Partner Detail </h2>
            </div>
            <div class="pull-right">
                <a class="btn btnSecondary" href="{{ route('partner.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        @if(!empty($partner->setting) && isset($partner->setting['logo']))
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Logo:</strong>
                    <img src="{{asset('/partner-logo/'.$partner->setting['logo'])}}" width="100px" height="100px">
                </div>
            </div>
        @endif
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>First Name:</strong>
                {{ $partner->partnerLogin->person ? $partner->partnerLogin->person->first_name : "N/A"}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Last Name:</strong>
                {{ $partner->partnerLogin->person ? $partner->partnerLogin->person->last_name : "N/A"}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Practice Name:</strong>
                {{ $partner->partnerLogin->person ? $partner->partnerLogin->person->practice_name : 'N/A' }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>DOB:</strong>
                {{ $partner->partnerLogin->person ? $partner->partnerLogin->person->dob : "N/A"}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Username:</strong>
                {{ $partner->partnerLogin ? $partner->partnerLogin->username : "N/A" }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                {{ $partner->partnerLogin ? $partner->partnerLogin->email  : "N/A" }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Phone:</strong>
                {{ $partner->partnerLogin->person ? $partner->partnerLogin->person->phone : 'N/A' }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Created By:</strong>
                {{ $partner->partnerLogin->createdBy ? $partner->partnerLogin->createdBy->first_name . ' ' . $partner->partnerLogin->createdBy->last_name : 'N/A' }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Updated By:</strong>
                {{ $partner->partnerLogin->createdBy ? $partner->partnerLogin->upDatedBy->first_name . ' ' . $partner->partnerLogin->upDatedBy->last_name : 'N/A' }}
            </div>
        </div>
        @if(count($roleNames) > 0)
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Role:</strong>
                    @foreach($roleNames as $name)
                        <label class="label label-success">{{ $name }}</label>
                    @endforeach
                </div>
            </div>
        @endif
        @if(!empty($partner->setting) && isset($partner->setting['pdf']))
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Pdfs:</strong>
                    @foreach($partner->setting['pdf'] as $pdf)
                        <a href="{{asset('partner-pdf/'.$pdf)}}">{{$pdf}} , </a>
                    @endforeach
                </div>
            </div>
        @endif
        @if(!empty($partner->partnerLogin->person->address1) )
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Address1:</strong>
                    {{$partner->partnerLogin->person->address1}}
                </div>
            </div>
        @endif
        @if(!empty($partner->partnerLogin->person->address2) )
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Address2:</strong>
                    {{$partner->partnerLogin->person->address2}}
                </div>
            </div>
        @endif
        @if(!empty($partner->partnerLogin->person->city) )
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>City:</strong>
                    {{$partner->partnerLogin->person->city}}
                </div>
            </div>
        @endif
        @if(!empty($partner->partnerLogin->person->state) )
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>State:</strong>
                    {{$partner->partnerLogin->person->state}}
                </div>
            </div>
        @endif
        @if(!empty($partner->partnerLogin->person->zip) )
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Zip:</strong>
                    {{$partner->partnerLogin->person->zip}}
                </div>
            </div>
        @endif
        @if(count($partner->groups) > 0)
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Groups:</strong>
                    @foreach($partner->groups as $group)
                        {{$group->groupLogin->person ? $group->groupLogin->person->practice_name : "N/A"}},
                    @endforeach
                </div>
            </div>
        @endif
    </div>
@endsection
