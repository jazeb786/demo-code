@extends('layouts.app')
@section('breadcrumbs')
    @include('partials.breadcrumb',['route' => request()->fullUrl(),'text' => 'Create Partner','pageLink' => request()->fullUrl() ,'iconClass' => "fas fa-swatchbook mdIcn"])
@endsection
@section('content')
    <!-- bannerSec -->
    <div class="bannerSec bannerSecII">
        <div class="txtWrap">
            <h1><i class="fas fa-user-plus mdIcn faicon"></i> Create Partner </h1>
        </div>
    </div>
    <!-- anmSection -->
    <div class="anmSection">
        <form action="{{route('partner.store')}}" class="anmForm" method="POST" id="formValidation" enctype="multipart/form-data">
            @csrf
            <div class="formBody">
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>First Name:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="first_name" value="{{old('first_name')}}" placeholder="First Name" required>
                        </div>
                        <div class="formCol d-block">
                            <label>Last Name:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="last_name" value="{{old('last_name')}}" placeholder="Last Name" required>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>Practice Name:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="practice_name" placeholder="Practice Name" required value="{{old('username')}}">
                        </div>
                        <div class="formCol d-block">
                            <label>Email:</label>
                            <input type="email" class="mdSelect inputValueCheck" name="email" value="{{old('email')}}" placeholder="admin@gmail.com" required>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>User Name:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="username" value="{{old('username')}}" placeholder="UserName" required>
                        </div>
                        <div class="formCol d-block">
                            <label>Password:</label>
                            <input type="password" class="mdSelect inputValueCheck" name="password" placeholder="********" required>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>Phone:</label>
                            <input type="phone" class="mdSelect inputValueCheck" name="phone" placeholder="2541369874" required>
                        </div>
                        <div class="formCol d-block">
                            <strong>Role:</strong>
                            <br/>
                            <select class="mdSelect inputValueCheck" name = "role" required>
                                <option value="">Select Role:</option>
                                @foreach($roles as $role)
                                    <option value="{{$role->name}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>Address1:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="address1" placeholder="address1" value="{{old('address1')}}">
                        </div>
                        <div class="formCol d-block">
                            <label>Address2:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="address2" placeholder="address2" value="{{old('address2')}}">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>City:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="city" placeholder="city" value="{{old('city')}}">
                        </div>
                        @include('partials.states')
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>Zip:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="zip" placeholder="84001" value="{{old('zip')}}">
                        </div>
                        <div class="formCol d-block">
                            <label>Dob:</label>
                            <input type="date" class="mdSelect inputValueCheck" name="dob" value="{{old('dob')}}" placeholder="Date of Birth">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>Logo:</label>
                            <input type="file" class="custom-file-input" id="customFile" name="logo" accept="image/x-png,image/gif,image/jpeg"  required>
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <div class="formCol d-block">
                            <label>Pdf:</label>
                            <input type="file" class="custom-file-input" name="pdf[]" id="customFile2" accept="application/pdf" multiple>
                            <label class="custom-file-label" for="customFile2">Choose file</label>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <strong>Groups:</strong>
                            <br/>
                            <select class="mdSelect inputValueCheck" name = "group_ids[]" multiple>
                                <option value="">Select Group:</option>
                                @foreach($groups as $group)
                                    <option value="{{$group->id}}">{{$group->groupLogin->person ? $group->groupLogin->person->practice_name : ''}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="formFooter">
                <!-- pagnitionFooter -->
                <div class="pagnitionFooter">
                    <div class="alignLeft">&nbsp;</div>
                    <div class="alignRight">
                        <button type="submit" class="btn btnSecondary">Create</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
