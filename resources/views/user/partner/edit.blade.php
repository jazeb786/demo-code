@extends('layouts.app')
@section('breadcrumbs')
    @include('partials.breadcrumb',['route' => request()->fullUrl(),'text' => 'Edit Partner','pageLink' => request()->fullUrl() ,'iconClass' => "fas fa-swatchbook mdIcn"])
@endsection
@section('content')
    <!-- bannerSec -->
    <div class="bannerSec bannerSecII">
        <div class="txtWrap">
            <h1><i class="fas fa-user-plus mdIcn faicon"></i> Edit Partner </h1>
        </div>
    </div>
    <!-- anmSection -->
    <div class="anmSection">
        <form action="{{route('partner.update',$partner->partnerLogin->id)}}" class="anmForm" method="POST" id="formValidation" enctype="multipart/form-data">
            {!! csrf_field() !!}
            {!! method_field('PUT') !!}
            <div class="formBody">
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>First Name:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="first_name" value="{{ $partner->partnerLogin->person ? $partner->partnerLogin->person->first_name : ""}}" placeholder="First Name" required>
                        </div>
                        <div class="formCol d-block">
                            <label>Last Name:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="last_name" value="{{$partner->partnerLogin->person ? $partner->partnerLogin->person->last_name : '' }}" placeholder="Last Name" required>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>Email:</label>
                            <input type="email" class="mdSelect inputValueCheck" name="email" value="{{$partner->partnerLogin ? $partner->partnerLogin->email  : ''}}" placeholder="admin@gmail.com" required>
                        </div>
                        <div class="formCol d-block">
                            <label>Practice Name:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="practice_name" value="{{$partner->partnerLogin->person ? $partner->partnerLogin->person->practice_name : ''}}" required>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>User Name:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="username" value="{{$partner->partnerLogin ? $partner->partnerLogin->username :  ''}}" placeholder="UserName" required>
                        </div>
                        <div class="formCol d-block">
                            <label>Password:</label>
                            <input type="password" class="mdSelect inputValueCheck" name="password" placeholder="********">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>Phone:</label>
                            <input type="phone" class="mdSelect inputValueCheck" name="phone" placeholder="2541369874" value="{{$partner->partnerLogin->person ? $partner->partnerLogin->person->phone : ''}}" required>
                        </div>
                        <div class="formCol d-block">
                            <strong>Role:</strong>
                            <br/>
                            <select class="mdSelect inputValueCheck" name = "role" required>
                                <option value="">Select Role:</option>
                                @foreach($roles as $role)
                                    @foreach($roleNames as $roleName)
                                        <option value="{{$role->name}}" @if($roleName == $role->name) selected @endif>{{$role->name}}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>Address1:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="address1" value="{{$partner->partnerLogin->person ? $partner->partnerLogin->person->address1 : ''}}" placeholder="address1">
                        </div>
                        <div class="formCol d-block">
                            <label>Address2:</label>
                            <input type="text" class="mdSelect inputValueCheck" name="address2" value="{{$partner->partnerLogin->person ? $partner->partnerLogin->person->address2 : ''}}"placeholder="address2">
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>City:</label>
                            <input type="text" class="mdSelect inputValueCheck" value="{{$partner->partnerLogin->person ? $partner->partnerLogin->person->city : ''}}"name="city" placeholder="city">
                        </div>
                        @include('partials.states')
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>Zip:</label>
                            <input type="text" class="mdSelect inputValueCheck" value="{{$partner->partnerLogin->person ? $partner->partnerLogin->person->zip : ''}}" name="zip" placeholder="84001">
                        </div>
                        <div class="formCol d-block">
                            <label>Dob:</label>
                            <input type="date" class="mdSelect inputValueCheck" name="dob" value="{{$partner->partnerLogin->person ? $partner->partnerLogin->person->dob : ''}}" placeholder="Date of Birth" required>
                        </div>
                    </div>
                </fieldset>
                @if(!empty($partner->setting))
                    <fieldset>
                        <div class="formInlineRow">
                            <div class="formCol d-block">
                                @if(!empty($partner->setting) && isset($partner->setting['logo']))
                                    <img src="{{asset('/partner-logo/'.$partner->setting['logo'])}}" width="100px" height="100px">
                                @endif
                            </div>
                            <div class="formCol d-block">
                                @if(!empty($partner->setting) && isset($partner->setting['pdf']))
                                    @foreach($partner->setting['pdf'] as $pdf)
                                        <a href="{{asset('partner-pdf/'.$pdf)}}">{{$pdf}} , </a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </fieldset>
                @endif
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <label>Logo:</label>
                            <input type="file" class="custom-file-input" id="customFile" name="logo" accept="image/x-png,image/gif,image/jpeg" @if(!isset($partner->setting['logo'])) required @endif>
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <div class="formCol d-block">
                            <label>Pdf:</label>
                            <input type="file" class="custom-file-input" name="pdf[]" id="customFile2" accept="application/pdf" multiple>
                            <label class="custom-file-label" for="customFile2">Choose file</label>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="formInlineRow">
                        <div class="formCol d-block">
                            <strong>Groups:</strong>
                            <br/>
                            <select class="mdSelect inputValueCheck" name = "group_ids[]" multiple>
                                <option value="">Select Group:</option>
                                @foreach($groups as $group)
                                    <option value="{{$group->id}}" @if($group->partner_id == $partner->id) selected @endif>{{$group->groupLogin->person ? $group->groupLogin->person->practice_name : ''}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="formFooter">
                <!-- pagnitionFooter -->
                <div class="pagnitionFooter">
                    <div class="alignLeft">&nbsp;</div>
                    <div class="alignRight">
                        <a href="{{route('partner.index')}}" class="btn btnPrimary">Cancel</a>
                        <button type="submit" class="btn btnSecondary">Update</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $("#force_reset").click(function () {
                if($(this).is(":checked")){
                    $(this).val("1");
                } else{
                    $(this).val("0");
                }
            });

        });
    </script>
@endpush
