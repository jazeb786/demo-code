@extends('layouts.app')
@section('breadcrumbs')
    @include('partials.breadcrumb',['route' => request()->fullUrl(),'text' => 'Partners','pageLink' => request()->fullUrl() , 'iconClass' => "fas fa-home mdIcn"])
@endsection
@section('content')
    <div class="bannerSec">
        <div class="txtWrap">
            <h1><i class="fas fa-users mdIcn faicon"></i> Partners </h1>
            <p>List of All Partners</p>
            @can('partner-create')
                <div class="header-elements">
                    <a href="{{route('partner.create')}}" class="btn btnSecondary">Create Partner</a>
                </div>
            @endcan
        </div>
        <!-- bannerFilterWrap -->
        <div class="bannerFilterWrap">
            <form class="filterForm" method="get" >
                <div class="formInlineRow">
                    <div class="formCol">
                        <label class="white-space-nowrap label" for="fnamedasboard">Search Partner by username:</label>
                        <div class="formCol"><input type="text" class="mdInput" placeholder="Search..." id="generalSearch" value="{{request()->get('search')}}" name="search"></div>
                        <button type="submit" class="btn btnIcn">Search</button> &nbsp;
                        <a href="{{route('partner.index')}}"  class="btn btnIcn" id="reset">Reset</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="pdTableSection">
        <div class="tableWrap">
            <div class="dstHead">
                <div class="dstDate">
                    <span>Partner (List View)</span>
                </div>
            </div>

            @if(count($partners) > 0)
                <table class="memTable mdyTable memDetTable margin-bottom-4">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>UserName</th>
                        <th>Practice Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Created By</th>
                        <th>Updated By</th>
                        @canany(['partner-create', 'partner-update', 'partner-show', 'partner-destroy'])
                            <th class="text-center">Actions</th>
                        @endcanany
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($partners as $key => $partner)
                        <tr>
                            <td>{{ $partner->partnerLogin->person ? $partner->partnerLogin->person->first_name . ' ' .$partner->partnerLogin->person->last_name : 'N/A'}}</td>
                            <td>{{ $partner->partnerLogin ? $partner->partnerLogin->username : 'N/A' }}</td>
                            <td>{{ $partner->partnerLogin->person ? $partner->partnerLogin->person->practice_name : 'N/A' }}</td>
                            <td>{{ $partner->partnerLogin ? $partner->partnerLogin->email : 'N/A' }}</td>
                            <td>{{ $partner->partnerLogin->person ? $partner->partnerLogin->person->phone : 'N/A' }}</td>
                            <td>{{ $partner->partnerLogin->createdBy ? $partner->partnerLogin->createdBy->first_name . ' ' . $partner->partnerLogin->createdBy->last_name : 'N/A' }}</td>
                            <td>{{ $partner->partnerLogin->upDatedBy ? $partner->partnerLogin->upDatedBy->first_name . ' ' . $partner->partnerLogin->upDatedBy->last_name : 'N/A' }}</td>
                            <td>
                                <ul class="tabActionList" style="max-width: 570px;text-align: center;margin: 0 auto;">
                                    @can('partner-show')
                                        <li><a href="{{route('partner.show',$partner->partnerLogin->id)}}" class="">View</a></li>
                                    @endcan
                                    @can('partner-update')
                                        <li><a href="{{route('partner.edit',$partner->partnerLogin->id)}}" class="">Edit</a></li>
                                    @endcan
                                    @can('partner-destroy')
                                        <li><a href="#m_modal_4_{{$partner->id}}" class="fancybox">Delete</a></li>
                                        @include('partials.deleteModal',['route'=>'partner.destroy','id' => $partner->id ])
                                    @endcan
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <center><h2>No Record Found</h2></center>
            @endif
            @if(count($partners) > 0)
                @php
                    $total = count($partners) *  $partners->currentPage();
                    if($partners->lastPage() == $partners->currentPage()){
                        $total = $partners->total();
                    }
                @endphp
                <div class="card card-manage">
                    <span class="float-left">Showing  {{ $partners->perPage() *  $partners->currentPage() - 9}} to {{ $total}} of {{$partners->total()}} records</span>
                    <div class="float-right">{{ $partners->appends(request()->all())->links('pagination::pagination-design')}}</div>
                </div>
            @endif
        </div>
    </div>
@endsection
