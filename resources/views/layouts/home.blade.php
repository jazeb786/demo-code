<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8" />
    <title>membersy | a membership experience</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.0/css/all.css">
    <link rel="icon" href="https://membersy.com/wp-content/uploads/2019/06/cropped-Logo-Membersy-Icon-social.header-1-192x192.jpg" sizes="192x192" />
    <link rel="apple-touch-icon" href="https://membersy.com/wp-content/uploads/2019/06/cropped-Logo-Membersy-Icon-social.header-1-180x180.jpg" />
    <meta name="msapplication-TileImage" content="https://membersy.com/wp-content/uploads/2019/06/cropped-Logo-Membersy-Icon-social.header-1-270x270.jpg" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- /global stylesheets -->
    @stack('stylesheet')
</head>
<body>
@yield('content')
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/jqueryCustomNew.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>
@stack('scripts')
</body>
</html>
