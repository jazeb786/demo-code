<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8" />
    <title>membersy | a membership experience</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.0/css/all.css">
    <link rel="icon" href="https://membersy.com/wp-content/uploads/2019/06/cropped-Logo-Membersy-Icon-social.header-1-192x192.jpg" sizes="192x192" />
    <link rel="apple-touch-icon" href="https://membersy.com/wp-content/uploads/2019/06/cropped-Logo-Membersy-Icon-social.header-1-180x180.jpg" />
    <meta name="msapplication-TileImage" content="https://membersy.com/wp-content/uploads/2019/06/cropped-Logo-Membersy-Icon-social.header-1-270x270.jpg" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    @stack('stylesheet')
</head>
<body>
<div id="pageWrapper">
    @include('partials.sidebar')
    <div class="offsetWrapper">
        <div class="innerWrapper">
            <div class="container">
                @include('partials.topbar')
                @include('partials.alerts')
                @yield('content')
            </div>
        </div>
    </div>
    @include('partials.footer')
</div>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="{{asset('js/scripts.js')}}"></script>
<script src="{{asset('js/functions.js')}}"></script>
<script src="{{asset('js/jqueryCustomNew.js')}}"></script>
@stack('scripts')
</body>
</html>
