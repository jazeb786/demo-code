<?php
/**
 * Created by PhpStorm.
 * User: AlphaRages
 * Date: 10/22/2020
 * Time: 9:54 PM
 */

namespace App\Utilities;


class Constant
{
    const type = [
        'partner' => 'partner',
        'group' => 'group',
        'region' => 'region',
        'location' => 'location',
        'admin' => 'admin'
    ];

    const Expire_Card = "The card you’ve entered is expired. <span class='bottomtext d-block'>Please try again using another card.</span>";
    const INCORRECT_CVC = "The card’s CVC number you’ve entered is incorrect. <span class='bottomtext d-block'>Please try again using the correct CVC number.</span>";
    const DO_NOT_HONOR = "The card you’ve entered has been declined for an unknown reason. <span class='bottomtext d-block'>Please try again using another card.</span>";
    const INCORRECT_NUMBER = "The credit/debit card number you’ve entered is incorrect. <span class='bottomtext d-block'>Please try again using the correct card number.</span>";
    const INSUFFICIENT_FUND = "The card you’ve entered has insufficient funds. <span class='bottomtext d-block'>Please try again using another card.</span>";
    const DECLINED_CARD = "The card you’ve entered has been declined for an unknown reason. <span class='bottomtext d-block'>Please try again using another card.</span>";
}
