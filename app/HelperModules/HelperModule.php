<?php

namespace App\HelperModules;

use App\Models\Group\Group;
use App\Models\Location\Location;
use App\Models\Partner\Partner;
use App\Models\Region\Region;
use App\Models\User\UserLogin;
use App\Utilities\Constant;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
/* @author <jazeb.mazher@alpharages.com> */
class HelperModule
{

    const SuccessCode = 200;
    const ErrorCode = 409;
    const SuccessMessage = 'Success';

    /**
     * @param $path
     * @param $fileName
     * @return array|bool|\Exception
     */
    static function uploadFile($path, $fileName)
    {
        if (!$path)
            return false;

        try {
            $dt = Carbon::parse(Carbon::Now());
            $timeStamp = $dt->timestamp;
            $destinationPath = public_path() . '/' .$path;
            $file = $fileName;
            $extension = $fileName->getClientOriginalExtension();
            $fileOriginalName = $fileName->getClientOriginalName();
            $fileOriginalName = str_replace('.' . $extension, "", $fileOriginalName);
            $fileName = $timeStamp . '_' . str_replace(" ","",$fileOriginalName) . '.' . $extension;
            $file->move($destinationPath, $fileName);
            return ['file' => $fileName];
        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @param $path
     */
    static function deleteFile($path)
    {
        File::delete($path);
    }

    /**
     * @param $type
     * @param null $message
     * @param null $data
     * @return \Illuminate\Support\Collection
     */
    static function jsonResponse($type, $message = null, $data = null)
    {
        $response['status'] = $type;
        $response['message'] = $message;
        $response['data'] = $data;

        return collect($response);
    }

    /**
     * @param $request
     * @return |null
     */
    static function checkParentType($request){
        $type = null;
        $model = null;
        if ($request->partner && $request->partner != 0 ){
            $model = Partner::findOrfail($request->partner);
            $type = Constant::type['partner'];
        }
        if ($request->group  && $request->group != 0) {
            $model = Group::findOrfail($request->group);
            $type = Constant::type['group'];
        }
        if ($request->region  && $request->region != 0){
            $model = Region::findOrfail($request->region);
            $type = Constant::type['region'];
        }
        if ($request->office  && $request->office != 0){
            $model = Location::findOrfail($request->office);
            $type = Constant::type['location'];
        }

        return ['type' => $type , 'model' => $model];
    }

    /**
     * @param $request
     * @return mixed
     */
    static function phoneRemoveSpace($request){
        $pattern = '/\s*/m';
        $replace = '';
        $message = $request->phone;
        $removedLinebaksAndWhitespace = preg_replace($pattern, $replace, $message);
        $firstReplace = str_replace('-', '', $removedLinebaksAndWhitespace);
        $secondReplace = str_replace('(', '', $firstReplace);
        $finalValue = str_replace(')', '', $secondReplace);
        return $finalValue;
    }

    /**
     * @return mixed
     */
    static function getLoginUserType(){
        return auth()->user()->type;
    }


    /**
     * @param $type
     * @return array
     */
    static function getTypeBaseDataForFilters($type){
        $partners = collect() ; $groups = collect();  $regions = collect();
        $locations = collect(); $plans = collect();
        $select = ['id','person_id','type','type_reference_id'];
        $select2 = ['id','practice_name'];
        if (Constant::type['admin'] == $type){
            $partners = Partner::partnerWithPerson($select,$select2)->get();
        } elseif(Constant::type['partner'] == $type){
            $groups = Group::where('partner_id',auth()->id())->groupWithPerson($select,$select2)->get();
        } elseif(Constant::type['group'] == $type){
            $regions = Region::where('group_id',auth()->id())->regionWithPerson($select,$select2)->get();
        } elseif(Constant::type['region'] == $type){
            $locations = Location::where('region_id',auth()->id())->locationWithPerson($select,$select2)->get();
        } elseif(Constant::type['location'] == $type){
            $location = Location::where('id',auth()->id())->with('plans')->first();
            if ($location)
                $plans = $location->plans;
        }
        $data = ['partners' => $partners , 'groups' => $groups ,
            'regions' => $regions , 'locations' => $locations , 'plans' => $plans];
        return $data;
    }

    /**
     * @param $e
     * @return string
     */
    static function getCustomMessage($e){
        $error = $e->getError();
        $message = $e->getMessage();
        $code = $error->code;
        if ($code == "invalid_expiry_month" ||  $code == "invalid_expiry_year" || $code == "expired_card"){
            $message  = Constant::Expire_Card;
        }
        if ($code == "incorrect_cvc"){
            $message  =  Constant::INCORRECT_CVC;
        }
        if ($code == "do_not_honor"){
            $message  = Constant::DO_NOT_HONOR;
        }
        if ($code == "incorrect_number"){
            $message  = Constant::INCORRECT_NUMBER;
        }
        if ($code == "insufficient_funds"){
            $message  = Constant::INSUFFICIENT_FUND;
        }
        if ($code == "service_not_allowed" || $code ==  "card_declined"){
            $message  = Constant::DECLINED_CARD;
        }
        return $message;
    }

}
