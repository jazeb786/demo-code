<?php

namespace App\HelperModules;

use App\Traits\Stripe\StripeCardToken;
use App\Traits\Stripe\StripeCustomer;
use App\Traits\Stripe\StripeSubscription;
use App\Traits\Stripe\StripeFile;
use App\Traits\Stripe\StripePlan;
use App\Traits\Stripe\StripeProduct;
use Stripe\Stripe;
/* @author <jazeb.mazher@alpharages.com> */
class StripeHelper
{
    use StripeProduct, StripeFile, StripePlan, StripeCustomer, StripeSubscription, StripeCardToken;

    public function __construct()
    {
       Stripe::setApiKey(env('STRIPE_SECRET'));
    }
}
