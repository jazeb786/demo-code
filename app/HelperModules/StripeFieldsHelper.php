<?php

namespace App\HelperModules;

use App\Models\Transaction;
use App\Models\User;
use App\Models\Product;
use App\Traits\StripeCustomer;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Stripe\Error\Card;
use Stripe\Stripe;
use Stripe\Customer;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Helper\Helper;

/* @author <jazeb.mazher@alpharages.com> */
class StripeFieldsHelper
{
    /**
     * StripeFieldsHelper constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param $request
     * @return array
     */
    static function card($request){
        return array(
            "number" => trim($request->credit_card),
            "exp_month" => trim($request->exp_month),
            "exp_year" => trim($request->exp_year),
            "cvc" => trim($request->security_code),
            "address_line1" => $request->address1,
            "name"=>$request->first_name.' '.$request->last_name
        );
    }

    /**
     * @param $request
     * @param $token
     * @param $user
     * @param $provider
     * @param $dso
     * @param $product
     * @return array
     */
    static function customerFields($request, $token, $user, $provider, $dso , $product){
        $firstName = $user->first_name;
        $lastName = $user->last_name;
        $email = $user->email;
        $paymentCheck = "Card";
        if (!empty($request->first_name) && !empty($request->last_name)){
            $firstName=  $request->first_name;
            $lastName = $request->last_name;
            $email = $request->email;
        }
        if ($request->payment_type == 1 || $request->check_member == "on"){
            $paymentCheck = "Cheque";
        }
        $coupon = $request->coupon;
        if ($dso->id == 56349){
            $coupon = 'DD522';
        }
        if ($dso->id == 56341){
            $coupon = 'SMILE50';
        }
        if ($request->payment_type == 2){
            $paymentCheck = "Cash";
        }
        $channel = HelperModule::platform($request);
        return array(
            'email' => strtolower($email),
            isset($token) ? ['source' => $token['data']['id']] : null,
            'description' => $product->name.' - ID # '.$user->id,
            'name' => $user->first_name . ' ' . $user->last_name,
            'phone' => $user->phone,
            'tax_exempt' => 'exempt',
            'address' => [
                'line1' => $request->address1,
                'state' => $request->state,
                'city' => $request->city,
                'country' => $request->country,
                'postal_code' => $request->zip,
            ],
            'shipping' => [
                'address' =>[
                    'line1' => $request->address1,
                    'state' => $request->state,
                    'city' => $request->city,
                    'country' => $request->country,
                    'postal_code' => $request->zip,
                ],
                'name' => ucfirst(strtolower($firstName)) .' ' . ucfirst(strtolower($lastName))
            ],
            'metadata' => array(
                "total_members" => $request->total_members,
                "dob"=>  Carbon::parse($request->dob)->toDateString(),
                "user_id" => $user->id,
                "provider_id" => $provider->id,
                "provider_name" => $provider->practice_name,
                "dso_id" => $request->dso_id,
                "dso_name" => $dso->practice_name,
                "payment_method" => $paymentCheck,
                'coupon' => $coupon,
                'Channel' => $channel,
                'Check Number' => $request->check_number,
                'Pms Number' => $request->pms_number
            )
        );
    }

    /**
     * @param $product
     * @param $invoice
     * @param $data
     * @param null $amount
     * @return float
     */
    static function transferPercentageCalculate($product, $invoice, $data , $amount = null){
        $invoiceTotal = $amount ;
        if (empty($amount))
            $invoiceTotal = $invoice['data']['object']['total'] / 100 ;

        $totalMembers = $data;
        $sumValues = 0;
        if ($totalMembers > 1){
            if ($product->application_percent == 1 ){
                $values = $product->application_fee / 100 * $product->price;
                if ($product->per_member_percent == 1){
                    $member = $totalMembers - 1 ;
                    $memberValue = $product->price_per_member / 100 * $product->additional_price_thrive;
                    if ($product->additional_member_check == 1){
                        $memberTotalValue = $memberValue * $member;
                        $sumValues = $memberTotalValue + $values;
                    } else{
                        $memberTotalValue = $memberValue;
                        $sumValues = $memberTotalValue + $values;
                    }
                } else{
                    $member = $totalMembers - 1 ;
                    $memberValue = $product->price_per_member;
                    if ($product->additional_member_check == 1){
                        $memberTotalValue = $memberValue * $member;
                        $sumValues = $memberTotalValue + $values;
                    } else{
                        $memberTotalValue = $memberValue;
                        $sumValues = $memberTotalValue + $values;
                    }
                }
            } else{
                $values = $product->application_fee;
                if ($product->per_member_percent == 1){
                    $member = $totalMembers - 1 ;
                    $memberValue = $product->price_per_member / 100 * $product->additional_price_thrive;
                    if ($product->additional_member_check == 1){
                        $memberTotalValue = $memberValue * $member;
                        $sumValues = $memberTotalValue + $values;
                    } else{
                        $memberTotalValue = $memberValue;
                        $sumValues = $memberTotalValue + $values;
                    }
                } else{
                    $member = $totalMembers - 1 ;
                    $memberValue = $product->price_per_member;
                    if ($product->additional_member_check == 1){
                        $memberTotalValue = $memberValue * $member;
                        $sumValues = $memberTotalValue + $values;
                    } else{
                        $memberTotalValue = $memberValue;
                        $sumValues = $memberTotalValue + $values;
                    }
                }
            }
        } else{
            if ($product->application_percent == 1 ) {
                $sumValues = $product->application_fee / 100 * $invoiceTotal;
            } else{
                $sumValues = $product->application_fee  ;
            }
        }
        if ($sumValues > $invoiceTotal){
            $invoiceMinus = $sumValues - $invoiceTotal;
        } else{
            $invoiceMinus = $invoiceTotal- $sumValues ;
        }

        $fpPercent =  $invoiceMinus * 100;
        $fP = round($fpPercent);
        return $fP;
    }

    /**
     * @param $customer
     * @param $provider
     * @param $request
     * @param $dso
     * @return array
     */
    static function subscriptionFields($customer, $provider, $request, $dso){
        $paymentCheck = "Credit Card";

        $coupon = $request->coupon;
        if ($dso->id == 56349){
            $coupon = 'DD522';
        }
        if ($dso->id == 56341){
            $coupon = 'SMILE50';
        }
        if ($request->payment_type == 1){
            $paymentCheck = "Cheque";
            $coupon = config('coupon.check_coupon');
        }
        if ($request->payment_type == 2){
            $paymentCheck = "Cash";
            $coupon = config('coupon.cach_coupon');
        }
        return array(
            'customer' => $customer['data']['id'],
            'coupon' => $coupon,
            'metadata' => array(
                "provider_id" => $provider->id,
                "provider_name" => ucfirst(strtolower($provider->practice_name)),
                "dso_id" => $dso->id,
                "dso_name" => ucfirst(strtolower($dso->practice_name)),
                "payment_method" => $paymentCheck,
                'user_id' => $customer['data']['user_id']
            ),
        );
    }

    /**
     * @param $user
     * @param $subscription
     * @param $status
     * @param int $userStatus
     * @param int $flag
     * @param int $platform
     * @return mixed
     */
    static function subscriptionUserFieldsUpdate($user , $subscription, $status , $userStatus = 8, $flag = 0 , $platform = 0){
        $user->status = 1;
        $user->remote_user_id = $user->id;
        $user->subscription_id = $subscription['data']['id'];
        $user->customer_id = $subscription['data']['customer'];
        if ($flag == 0){
            $user->became_active = date("Y-m-d h:i:s", $subscription['data']['current_period_start']);
            $user->expire_date = date("Y-m-d h:i:s", $subscription['data']['current_period_end']);
        }
        $user->sale_force_id = '';
        $user->update();
        if ($status != 6 &&  $status != 17)
            HelperModule::activity($user,$status,$subscription,0,1,$platform);

//        HelperModule::NewUserIdInRedCarts($user);
        return $user;
    }


}
