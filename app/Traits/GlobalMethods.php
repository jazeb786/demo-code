<?php


namespace App\Traits;


trait GlobalMethods
{
    /**
     * @param $permission
     * @param string[] $select
     * @return mixed
     */
    public static function basicInfo($permission, $select = ['*'])
    {
        return self::select($select)->when(!auth()->user()->can($permission), fn($q) => $q->where('created_by', auth()->id()));
    }
}
