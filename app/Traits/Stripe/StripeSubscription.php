<?php


namespace App\Traits\Stripe;


use App\HelperModules\HelperModule;
use App\HelperModules\StripeFieldsHelper;
use Stripe\Subscription;
use Stripe\Exception\ApiErrorException;
use Stripe\Exception\CardException;
use Stripe\Exception\InvalidRequestException;
trait StripeSubscription{


    /**
     * @param $product
     * @param $request
     * @param $provider
     * @param $dso
     * @param $arr
     * @param $customer
     * @param $plan
     * @return \Illuminate\Support\Collection
     */
    public function subscription($product, $request , $provider, $dso , $arr , $customer , $plan){
        try{
            $subscription = Subscription::create(array(
                StripeFieldsHelper::subscriptionFields($customer, $provider, $request, $dso),
                'items' => [[
                    'plan' => $plan['id'],
                    'quantity' => $request->total_members,
                ]],
            ));
            return HelperModule::jsonResponse(HelperModule::SuccessCode,"success",$subscription);
        } catch (CardException $e) {
            $error = $e->getMessage();
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$error);
        } catch (InvalidRequestException $e){
            $error = $e->getMessage();
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$error);
        } catch(\Exception $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode, $e->getMessage());
        }
    }

    /**
     * @param $dso
     * @param $product
     * @param $user
     * @return \Illuminate\Support\Collection
     */
    public function retrieveSubscription($dso, $product, $user){
        try{
            $subscription = Subscription::retrieve(($user->subscription_id));
            return HelperModule::jsonResponse(HelperModule::SuccessCode, "",$subscription);
        } catch(InvalidRequestException $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode, $e->getMessage());
        } catch(\Exception $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$e->getMessage());
        }
    }
}
