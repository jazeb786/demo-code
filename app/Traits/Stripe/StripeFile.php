<?php


namespace App\Traits\Stripe;


use App\HelperModules\HelperModule;
use Stripe\Exception\InvalidRequestException;
use Stripe\File;
use Stripe\FileLink;
trait StripeFile
{
    /**
     * @param $file
     * @return \Illuminate\Support\Collection
     */
    public function file($file){
        try{
            $fp = fopen($file, 'r');
            $fileData = File::create(['purpose' => 'business_logo','file' => $fp]);
            $fileLink = FileLink::create(['file' => $fileData->id]);
            $url = $fileLink->url;
            return HelperModule::jsonResponse(HelperModule::SuccessCode,HelperModule::SuccessMessage,$url);
        } catch (InvalidRequestException $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$e->getMessage());
        } catch (\Exception $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$e->getMessage());
        }

    }
}
