<?php


namespace App\Traits\Stripe;


use App\HelperModules\HelperModule;
use App\HelperModules\StripeFieldsHelper;
use Stripe\Customer;
use Stripe\Exception\CardException;
use Stripe\Exception\InvalidRequestException;
trait StripeCustomer{

    /**
     * @param $customer
     * @return \Illuminate\Support\Collection
     */
    public function retrieve($customer){
        try{
            $customerRetrieve = Customer::retrieve($customer);
            return HelperModule::jsonResponse(HelperModule::SuccessCode,"success",$customer);
        }  catch (CardException $e) {
            return HelperModule::jsonResponse(HelperModule::ErrorCode,HelperModule::getCustomMessage($e));
        } catch (InvalidRequestException $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode,HelperModule::getCustomMessage($e));
        } catch (\Exception $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode,HelperModule::getCustomMessage($e));
        }
    }


    /**
     * @param $request
     * @param $token
     * @param $user
     * @param $provider
     * @param $dso
     * @param $product
     * @return \Illuminate\Support\Collection
     */
    public function create($request, $token, $user, $provider, $dso , $product){
        try{
            $customer = null;
            $customer  = Customer::create(array(
                StripeFieldsHelper::customerFields($request, $token, $user, $provider, $dso , $product),
            ));
            return HelperModule::jsonResponse(HelperModule::SuccessCode,"success",$customer);
        }  catch (CardException $e) {
            return HelperModule::jsonResponse(HelperModule::ErrorCode,HelperModule::getCustomMessage($e));
        } catch (InvalidRequestException $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode,HelperModule::getCustomMessage($e));
        } catch (\Exception $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode,HelperModule::getCustomMessage($e));
        }
    }

    /**
     * @param $user
     * @return \Illuminate\Support\Collection
     */
    public function delete($user){
        try{
            $customer = Customer::retrieve(
                $user->customer_id
            );
            $customer->delete();
            return HelperModule::jsonResponse(200,"success",$customer);
        } catch (InvalidRequestException $e){
            $error = $e->getMessage();
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$error);
        } catch (\Exception $e){
            $error = $e->getMessage();
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$error);
        }
    }
}
