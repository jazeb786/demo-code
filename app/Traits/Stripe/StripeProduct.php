<?php


namespace App\Traits\Stripe;


use App\HelperModules\HelperModule;
use Stripe\Exception\InvalidRequestException;
use Stripe\Product;

trait StripeProduct
{
    /**
     * @param $name
     * @param array $images
     * @return \Illuminate\Support\Collection
     */
    public function productCreate($name , $images = []){
        try{
            $product = Product::create(['name' => $name,'images' => $images]);
            return HelperModule::jsonResponse(HelperModule::SuccessCode,HelperModule::SuccessMessage,$product);
        } catch (InvalidRequestException $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$e->getMessage());
        } catch (\Exception $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$e->getMessage());
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public function productDelete($id){
        try{
            $product = Product::retrieve($id);
            $product->delete();
            return HelperModule::jsonResponse(HelperModule::SuccessCode,HelperModule::SuccessMessage,$product);
        } catch (InvalidRequestException $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$e->getMessage());
        } catch (\Exception $e){
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$e->getMessage());
        }

    }
}
