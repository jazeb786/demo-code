<?php


namespace App\Traits\Stripe;


use App\HelperModules\HelperModule;
use App\HelperModules\StripeFieldsHelper;
use Stripe\Exception\CardException;
use Stripe\Exception\InvalidRequestException;
use Stripe\Token;
trait StripeCardToken{
    /**
     * @param $request
     * @return \Illuminate\Support\Collection
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function token($request){
        try{
            $token = Token::create(array(
                "card" => StripeFieldsHelper::card($request),
            ));
            return HelperModule::jsonResponse(200,"success",$token);
        } catch (CardException $e) {
            return HelperModule::jsonResponse(409,HelperModule::getCustomMessage($e));
        } catch (InvalidRequestException $e){
            return HelperModule::jsonResponse(409,HelperModule::getCustomMessage($e));
        }
    }
}
