<?php


namespace App\Traits\Stripe;


use App\HelperModules\HelperModule;
use Stripe\Exception\InvalidRequestException;
use Stripe\Price;

trait StripePlan
{

    /**
     * @param $tiers
     * @param $productId
     * @param $interval
     * @param $name
     * @return \Illuminate\Support\Collection
     */
    public function planCreate($tiers, $productId, $interval , $name){
        try{
            $plan = Price::create([
                'product' => $productId,
                'recurring' => [
                    'interval' => $interval
                ],
                'tiers_mode' => 'graduated',
                "currency" => "usd",
                "billing_scheme" => "tiered",
                "nickname" => $name,
                "tiers" => $tiers
            ]);
            return HelperModule::jsonResponse(HelperModule::SuccessCode,HelperModule::SuccessMessage,$plan);
        } catch (InvalidRequestException $e){
            $this->productDelete($productId);
            dd($e->getMessage());
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$e->getMessage());
        } catch (\Exception $e){
            $this->productDelete($productId);
            dd($e->getMessage());
            return HelperModule::jsonResponse(HelperModule::ErrorCode,$e->getMessage());
        }

    }
}
