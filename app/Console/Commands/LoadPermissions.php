<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;

class LoadPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:permission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $routes = Route::getRoutes();
        $bar = $this->getOutput()->createProgressBar(count($routes));
        foreach ($routes as $val) {
            if (isset($val->action['middleware'])) {
                $middleware = $val->action['middleware'][count($val->action['middleware']) - 1];
                if (strchr($middleware, 'permission.check')) {
                    $permissions = explode(':', $middleware);
                    if (count($permissions) > 0) {
                        $permission = $permissions[count($permissions) - 1];
                        if ($permission) {
                            if (!Permission::where('name', $permission)->exists())
                                Permission::create(['name' => $permission]);
                        }
                    }
                }
            }
            $bar->advance();
        }
    }
}
