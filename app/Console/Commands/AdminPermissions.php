<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:permission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $role = Role::where('name', 'admin')->first();
        $alreadyAssignPermissions = $role->permissions()->pluck('id');
        $permissions = Permission::whereNotIn('id', $alreadyAssignPermissions)->pluck('id');
        if ($permissions)
            $role->givePermissionTo($permissions);
    }
}
