<?php

namespace App\Http\Controllers\User;

use App\HelperModules\HelperModule;
use App\Http\Controllers\Controller;
use App\Models\Group\Group;
use App\Models\Partner\Partner;
use App\Models\User\UserLogin;
use App\Utilities\Constant;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Events\UserLogin as UserLoginEvent;
use Spatie\Permission\Models\Role;

class PartnerController extends Controller
{
    private array $selectLogin;
    private array $select;
    private Partner $partnerObj;

    /**
     * PartnerController constructor.
     */
    public function __construct()
    {
        $this->select = ['*'];
        $this->selectLogin = ['id', 'username', 'person_id', 'type_reference_id', 'email', 'type', 'created_by', 'updated_by'];
        $this->partnerObj = new Partner();
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|View|Response
     */
    public function index(Request $request)
    {
        $loginId = auth()->id();
        $partners = $this->instance()->whereHas('partnerLogin', fn($q) => $q->where('id', '<>', $loginId)
            ->when(!empty($request->search), fn($q1) => $q1->where('username', $request->search)))
            ->orderBy('id', 'DESC')->partnerLogin($this->selectLogin)
            ->paginate(10);
        return view('user.partner.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        $roles = Role::get();
        $groups = Group::with('groupLogin.person')->get();
        $state = null;
        return view('user.partner.create', get_defined_vars());
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->partnerObj->rules());
            if ($validator->fails())
                return redirect()->route('partner.create')->with('error', $validator->errors()->first())->withInput();

            DB::beginTransaction();
            $collection = $this->uploadPdfAndLogo($request);
            $request->request->add(['case' => 'create', 'created_by' => auth()->id(), 'updated_by' => auth()->id(),
                'type' => Constant::type['partner'], 'setting' => $collection]);
            $partner = $this->partnerObj->create($request->only('setting'));
            event(new UserLoginEvent($partner, $request));
            if (count($request->group_ids)) {
                foreach ($request->group_ids as $group_id) {
                    Group::where('id', $group_id)->update(['partner_id' => $partner->id]);
                }
            }
            DB::commit();
            return redirect()->route('partner.index')->with('success', 'Partner created successfully');
        } catch (Exception $e) {
            return redirect()->route('partner.create')->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|RedirectResponse|Response
     */
    public function show(int $id)
    {
        try {
            $loginId = auth()->id();
            $partner = $this->instance()->whereHas('partnerLogin', fn($q) => $q->where('id', $id))
                ->partnerLogin($this->selectLogin)->assignGroup()->first();

            $roleNames = $partner->partnerLogin ? $partner->partnerLogin->getRoleNames() : [];
            return view('user.partner.show', get_defined_vars());
        } catch (Exception $e) {
            return redirect()->back()
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|RedirectResponse|Response
     */
    public function edit(int $id)
    {
        try {
            $loginId = auth()->id();
            $partner = $this->instance()->whereHas('partnerLogin', fn($q) => $q->where('id', $id))
                ->partnerLogin($this->selectLogin)->first();

            $state = $partner->partnerLogin->person ? $partner->partnerLogin->person->state : null;
            $roles = Role::all();
            $roleNames = $partner->partnerLogin ? $partner->partnerLogin->getRoleNames() : [];
            $groups = Group::with('groupLogin.person')->get();
            return view('user.partner.edit', get_defined_vars());
        } catch (Exception $e) {
            return redirect()->back()
                ->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse|Response
     */
    public function update(Request $request, int $id)
    {
        try {
            $rules = $this->partnerObj->rules();
            $rules['username'][1] = $rules['username'][1]->ignore($id, 'id');
            $rules['email'][1] = $rules['email'][1]->ignore($id, 'id');
            unset($rules['password'], $rules['logo']);
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return redirect()->back()->with('error', $validator->errors()->first())->withInput();

            DB::beginTransaction();
            if (empty($request->password)) {
                $request->request->remove('password');
            }
            $userLogin = UserLogin::findOrFail($id);
            if (!empty($request->pdf) && !empty($userLogin->partner) && isset($userLogin->partner->setting['pdf'])) {
                foreach ($userLogin->partner->setting['pdf'] as $pdf) {
                    HelperModule::deleteFile(public_path('partner-pdf/' . $pdf));
                }
            }
            if (!empty($request->logo) > 0 && !empty($userLogin->partner) && isset($userLogin->partner->setting['logo'])) {
                HelperModule::deleteFile(public_path('partner-logo/' . $userLogin->partner->setting['logo']));
            }
            $collection = $this->uploadPdfAndLogo($request, $userLogin->partner->setting);
            $request->request->add(['case' => 'update', 'updated_by' => auth()->id(), 'setting' => $collection]);
            $userLogin->partner()->update($request->only('setting'));
            event(new UserLoginEvent($userLogin, $request));
            if (isset($request->group_ids)) {
                Group::whereIn('id', $userLogin->partner->groups->pluck('id'))->update(['partner_id' => 0]);
                foreach ($request->group_ids as $group_id) {
                    Group::where('id', $group_id)->update(['partner_id' => $userLogin->partner->id]);
                }
            }
            DB::commit();
            return redirect()->route('partner.index')
                ->with('success', 'Partner updated successfully');
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse|Response
     */
    public function destroy(int $id)
    {
        try {
            DB::beginTransaction();
            $this->instance()->findOrFail($id)->delete();
            DB::commit();
            return redirect()->route('partner.destroy')
                ->with('success', 'Partner deleted successfully');
        } catch (Exception $e) {
            return redirect()->back()
                ->with('error', $e->getMessage());
        }
    }

    /**
     * @param $request
     * @param $partner
     * @return Collection
     */
    public function uploadPdfAndLogo($request, $partner = null)
    {
        $collection = collect();
        if (empty($request->pdf)) {
            if (!empty($partner) && isset($partner['pdf'])) {
                $collection->put('pdf', $partner['pdf']);
            }
        }
        if (!empty($request->pdf)) {
            $arr = [];
            foreach ($request->pdf as $pdf) {
                $data = HelperModule::uploadFile('partner-pdf', $pdf);
                $arr[] = $data['file'];
            }
            $collection->put('pdf', $arr);
        }
        $logo = [];
        if (!empty($request->logo))
            $logo = HelperModule::uploadFile('partner-logo', $request->logo);

        if (!empty($partner) && isset($partner['logo']))
            $collection->put('logo', $partner['logo']);
        if (count($logo) > 0)
            $logo ? $collection->put('logo', $logo['file']) : [];

        return $collection;
    }

    /**
     * @return mixed
     */
    private function instance()
    {
        return $this->partnerObj->basicInfo($this->select);
    }
}
