<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PermissionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param  $permission
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $permission)
    {
        if (auth()->user() && auth()->user()->hasRole('Admin')) {
            return $next($request);
        } elseif (auth()->user() && !auth()->user()->can($permission) &&
            !auth()->user()->hasRole('Admin')) {
            abort(401);
        }
        return $next($request);
    }
}
