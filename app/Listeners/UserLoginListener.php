<?php

namespace App\Listeners;

use App\Events\UserLogin;
use App\Models\Person\Person;
use App\Models\User\UserLogin as Login;
use Illuminate\Support\Facades\Log;

class UserLoginListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserLogin $event
     * @return void
     */
    public function handle(UserLogin $event)
    {
        switch ($event->request->case) {
            case 'create':
                $person = Person::create($event->request->only('first_name', 'last_name', 'phone', 'dob', 'address1', 'address2', 'city', 'state', 'zip', 'practice_name', 'employee_id'));
                $event->request->request->add(['person_id' => $person->id]);
                $userLoginModel = $event->object->login()->create($event->request->only(['username', 'password', 'email', 'type', 'person_id', 'created_by', 'updated_by']));
                $userLoginModel->assignRole($event->request->role);
                break;
            case 'update':
                $data = $event->request->only(['username', 'password', 'email', 'updated_by']);
                if (!empty($event->request->type_reference_id) && !empty($event->request->type))
                    $data = $event->request->only(['username', 'password', 'email', 'updated_by', 'type', 'type_reference_id']);

                $event->object->person()->update($event->request->only('first_name', 'last_name', 'phone', 'dob', 'address1', 'address2', 'city', 'state', 'zip', 'practice_name', 'employee_id'));
                $event->object->update($data);
                $event->object->roles()->detach();
                $event->object->assignRole($event->request->role);
                $event->object->forgetCachedPermissions();
                break;
        }
    }
}
