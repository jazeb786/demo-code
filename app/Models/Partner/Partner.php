<?php

namespace App\Models\Partner;

use App\Models\Partner\traits\Relations;
use App\Models\Partner\traits\Scopes;
use App\Models\Partner\traits\Validations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use HasFactory, Relations, Scopes, Validations;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'setting',
        'status'
    ];

    protected $casts = ['setting' => 'collection'];

    /**
     *
     */
    public static function boot()
    {
        parent::boot();

        self::deleting(function (Partner $partner) {
            $partner->partnerLogin->person()->delete();
            $partner->partnerLogin()->delete();
        });
    }

    /**
     * @param string[] $select
     * @return mixed
     */
    public static function basicInfo($select = ['*'])
    {
        return self::select($select)->when(!auth()->user()->can(config('staticpermissions.super-admin.partner-list')), fn($q) => $q->whereHas('login', fn($q) => $q->where('created_by', auth()->id())));
    }

}
