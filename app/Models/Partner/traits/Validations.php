<?php

namespace App\Models\Partner\traits;

use Illuminate\Validation\Rule;
trait Validations
{

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'username' => ['required', Rule::unique('user_logins')],
            'email' => ['required', Rule::unique('user_logins')],
            'first_name' => 'required',
            'last_name' => 'required',
            'practice_name' => 'required',
            'phone' => 'required',
            'dob' => 'required',
            'password' => 'required|min:8',
            'role' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

}
