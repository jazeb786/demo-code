<?php

namespace App\Models\Partner\traits;
trait Scopes
{

    /**
     * @param $query
     * @param string $select
     * @param string $select1
     * @param string $select2
     * @return mixed
     */
    public function scopePartnerLogin($query, $select ='*' , $select1 = '*', $select2 = '*', $select3 = '*')
    {
        return $query->with('partnerLogin',fn($q) => $q->select($select)
        ->with('createdBy',fn($q1) => $q1->select($select1))
            ->with('updatedBy',fn($q2) => $q2->select($select2))
            ->with('person',fn($q3) => $q3->select($select3)));
    }


    /**
     * @param $query
     * @param string $select
     * @param string $select1
     * @param string $select2
     * @return mixed
     */
    public function scopeAssignGroup($query, $select ='*' , $select1 = '*', $select2 = '*')
    {
        return $query->with('groups',fn($q) => $q->select($select)
        ->with('groupLogin',fn($q1) => $q1->select($select1)
        ->with('person',fn($q2) => $q2->select($select2))));
    }

    /**
     * @param $query
     * @param string $select
     * @param string $select1
     * @return mixed
     */
    public function scopePartnerWithPerson($query, $select ='*' , $select1 = '*')
    {
        return $query->with('partnerLogin',fn($q) => $q->select($select)
        ->with('person',fn($q1) => $q1->select($select1)->orderBy('practice_name')));
    }

}
