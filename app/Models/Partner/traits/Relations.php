<?php

namespace App\Models\Partner\traits;

use App\Models\Group\Group;
use App\Models\User\UserLogin;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
trait Relations
{

    /**
     * @return HasOne
     */
    public function partnerLogin(): HasOne
    {
        return $this->hasOne(UserLogin::class, 'type_reference_id', 'id')->where('type','partner');
    }

    /**
     * @return HasMany
     */
    public function login(): HasMany
    {
        return $this->HasMany(UserLogin::class, 'type_reference_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function groups(): HasMany
    {
        return $this->hasMany(Group::class, 'partner_id', 'id');
    }

}
