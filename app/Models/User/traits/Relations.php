<?php

namespace App\Models\User\traits;

use App\Models\Group\Group;
use App\Models\Location\Location;
use App\Models\Partner\Partner;
use App\Models\Person\Person;
use App\Models\Region\Region;
use Illuminate\Database\Eloquent\Relations\HasOne;
trait Relations
{

    /**
     * @return HasOne
     */
    public function createdBy(): HasOne
    {
        return $this->hasOne(Person::class, 'id', 'created_by');
    }

    /**
     * @return HasOne
     */
    public function updatedBy(): HasOne
    {
        return $this->hasOne(Person::class, 'id', 'updated_by');
    }

    /**
     * @return HasOne
     */
    public function person(): HasOne
   {
       return $this->hasOne(Person::class, 'id', 'person_id');
   }

    /**
     * @return HasOne
     */
    public function partner(): HasOne
    {
        return $this->hasOne(Partner::class, 'id', 'type_reference_id');
    }

    /**
     * @return HasOne
     */
    public function group(): HasOne
    {
        return $this->hasOne(Group::class, 'id', 'type_reference_id');
    }

    /**
     * @return HasOne
     */
    public function region(): HasOne
    {
        return $this->hasOne(Region::class, 'id', 'type_reference_id');
    }

    /**
     * @return HasOne
     */
    public function location(): HasOne
    {
        return $this->hasOne(Location::class, 'id', 'type_reference_id');
    }

}
