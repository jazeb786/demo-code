<?php

namespace App\Models\User;

use App\Models\User\traits\Relations;
use App\Traits\GlobalMethods;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class UserLogin extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles, Relations, GlobalMethods;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'person_id',
        'type_reference_id',
        'email',
        'password',
        'type',
        'status',
        'force_reset_password',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();
        self::deleting(fn(UserLogin $userLogin) => $userLogin->person()->delete());
    }

}
