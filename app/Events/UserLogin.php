<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;

class UserLogin
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public Request $request;
    public $object;

    /**
     * Create a new event instance.
     *
     * @param $object
     * @param Request $request
     */
    public function __construct($object, Request $request)
    {
        $this->object = $object;
        $this->request = $request;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
