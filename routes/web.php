<?php

use App\Http\Controllers\AjaxController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\Plan\PlanController;
use App\Http\Controllers\User\AdminController;
use App\Http\Controllers\User\GroupController;
use App\Http\Controllers\User\MemberController;
use App\Http\Controllers\User\PartnerController;
use App\Http\Controllers\User\RegionController;
use App\Http\Controllers\User\RoleController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\User\UserManagementController;
use Illuminate\Support\Facades\Route;

Route::get('/', fn() => redirect()->to('/login'));

// force reset password form route
Route::get('/force/reset', [UserController::class, 'resetForm']
)->name('force.reset.form')->middleware('auth');
Route::post('/password/update', [UserController::class, 'passwordUpdate'])->name('password.update')->middleware('auth');

Route::middleware(['auth', 'force.reset'])->group(fn() => [
    // User Dashboard
    Route::get('/dashboard', [UserController::class, 'index'])->name('dashboard'),

    // Roles Routes
    Route::group(['prefix' => 'roles'], fn() => [
        Route::get('/', [RoleController::class, 'index'])->middleware(['permission.check:role-list'])->name('roles.index'),

        Route::get('/create', [RoleController::class, 'create'])->middleware(['permission.check:role-create'])->name('roles.create'),

        Route::post('/create', [RoleController::class, 'store'])->middleware(['permission.check:role-create'])->name('roles.create'),

        Route::get('/{role}/edit', [RoleController::class, 'edit'])->middleware(['permission.check:role-update'])->name('roles.update'),

        Route::put('/{role}/edit', [RoleController::class, 'update'])->middleware(['permission.check:role-update'])->name('roles.update'),

        Route::get('/{role}', [RoleController::class, 'show'])->middleware(['permission.check:role-show'])->name('roles.show'),

        Route::get('destroy/{role_id}', [RoleController::class, 'destroy'])->middleware(['permission.check:role-destroy'])->name('roles.destroy')
    ]),

    // Admin Create Routes
    Route::group(['prefix' => 'admin'], fn() => [

        Route::get('/', [AdminController::class, 'index'])->middleware(['permission.check:admin-list'])->name('admin.index'),

        Route::get('/create', [AdminController::class, 'create'])->middleware(['permission.check:admin-create'])->name('admin.create'),

        Route::post('/create', [AdminController::class, 'store'])->middleware(['permission.check:admin-create'])->name('admin.create'),

        Route::get('/{admin}/edit', [AdminController::class, 'edit'])->middleware(['permission.check:admin-update'])->name('admin.edit'),

        Route::put('/{admin}/edit', [AdminController::class, 'update'])->middleware(['permission.check:admin-update'])->name('admin.update'),

        Route::get('/{admin}', [AdminController::class, 'show'])->middleware(['permission.check:admin-show'])->name('admin.show'),

        Route::get('destroy/{admin_id}', [AdminController::class, 'destroy'])->middleware(['permission.check:admin-destroy'])->name('admin.destroy')
    ]),

    // Partner Routes
    Route::group(['prefix' => 'partner'], fn() => [

        Route::get('/', [PartnerController::class, 'index'])->middleware(['permission.check:partner-list'])->name('partner.index'),

        Route::get('/create', [PartnerController::class, 'create'])->middleware(['permission.check:partner-create'])->name('partner.create'),

        Route::post('/create', [PartnerController::class, 'store'])->middleware(['permission.check:partner-create'])->name('partner.store'),

        Route::get('/{partner}/edit', [PartnerController::class, 'edit'])->middleware(['permission.check:partner-update'])->name('partner.edit'),

        Route::put('/{partner}/edit', [PartnerController::class, 'update'])->middleware(['permission.check:partner-update'])->name('partner.update'),

        Route::get('/{partner}', [PartnerController::class, 'show'])->middleware(['permission.check:partner-show'])->name('partner.show'),

        Route::get('destroy/{partner_id}', [PartnerController::class, 'destroy'])->middleware(['permission.check:partner-destroy'])->name('partner.destroy')
    ]),

    // Group Routes
    Route::group(['prefix' => 'group'], fn() => [

        Route::get('/', [GroupController::class, 'index'])->middleware(['permission.check:group-list'])->name('group.index'),

        Route::get('/create', [GroupController::class, 'create'])->middleware(['permission.check:group-create'])->name('group.create'),

        Route::post('/create', [GroupController::class, 'store'])->middleware(['permission.check:group-create'])->name('group.store'),

        Route::get('/{group}/edit', [GroupController::class, 'edit'])->middleware(['permission.check:group-update'])->name('group.edit'),

        Route::put('/{group}/edit', [GroupController::class, 'update'])->middleware(['permission.check:group-update'])->name('group.update'),

        Route::get('/{group}', [GroupController::class, 'show'])->middleware(['permission.check:group-show'])->name('group.show'),

        Route::get('destroy/{group_id}', [GroupController::class, 'destroy'])->middleware(['permission.check:group-destroy'])->name('group.destroy')
    ]),

    // Region Routes
    Route::group(['prefix' => 'region'], fn() => [

        Route::get('/', [RegionController::class, 'index'])->middleware(['permission.check:region-list'])->name('region.index'),

        Route::get('/create', [RegionController::class, 'create'])->middleware(['permission.check:region-create'])->name('region.create'),

        Route::post('/create', [RegionController::class, 'store'])->middleware(['permission.check:region-create'])->name('region.store'),

        Route::get('/{region}/edit', [RegionController::class, 'edit'])->middleware(['permission.check:region-update'])->name('region.edit'),

        Route::put('/{region}/edit', [RegionController::class, 'update'])->middleware(['permission.check:region-update'])->name('region.update'),

        Route::get('/{region}', [RegionController::class, 'show'])->middleware(['permission.check:region-show'])->name('region.show'),

        Route::get('destroy/{region_id}', [RegionController::class, 'destroy'])->middleware(['permission.check:region-destroy'])->name('region.destroy')
    ]),

    // Location Routes
    Route::group(['prefix' => 'location'], fn() => [

        Route::get('/', [LocationController::class, 'index'])->middleware(['permission.check:location-list'])->name('location.index'),

        Route::get('/create', [LocationController::class, 'create'])->middleware(['permission.check:location-create'])->name('location.create'),

        Route::post('/create', [LocationController::class, 'store'])->middleware(['permission.check:location-create'])->name('location.store'),

        Route::get('/{location}/edit', [LocationController::class, 'edit'])->middleware(['permission.check:location-update'])->name('location.edit'),

        Route::put('/{location}/edit', [LocationController::class, 'update'])->middleware(['permission.check:location-update'])->name('location.update'),

        Route::get('/{location}', [LocationController::class, 'show'])->middleware(['permission.check:location-show'])->name('location.show'),

        Route::get('destroy/{location_id}', [LocationController::class, 'destroy'])->middleware(['permission.check:location-destroy'])->name('location.destroy')]),

    // User Management Routes
    Route::group(['prefix' => 'user'], fn() => [

        Route::get('/', [UserManagementController::class, 'index'])->middleware(['permission.check:user-list'])->name('user.index'),

        Route::get('/create', [UserManagementController::class, 'create'])->middleware(['permission.check:user-create'])->name('user.create'),

        Route::post('/create', [UserManagementController::class, 'store'])->middleware(['permission.check:user-create'])->name('user.store'),

        Route::get('/{user}/edit', [UserManagementController::class, 'edit'])->middleware(['permission.check:user-update'])->name('user.edit'),

        Route::put('/{user}/edit', [UserManagementController::class, 'update'])->middleware(['permission.check:user-update'])->name('user.update'),

        Route::get('/{user}', [UserManagementController::class, 'show'])->middleware(['permission.check:user-show'])->name('user.show'),

        Route::get('destroy/{user_id}', [UserManagementController::class, 'destroy'])->middleware(['permission.check:user-destroy'])->name('user.destroy')
    ]),

    //Ajax Request Routes
    Route::group(['prefix' => 'ajax'], fn() => [
        Route::get('/partner/{partner_id}', [AjaxController::class, 'partner']),

        Route::get('/group/{group_id}', [AjaxController::class, 'group']),

        Route::get('/region/{region_id}', [AjaxController::class, 'region']),

        Route::get('/location/{location_id}', [AjaxController::class, 'location']),

        Route::get('/plan/{plan_id}', [AjaxController::class, 'plan'])

    ]),

    // Plan Routes
    Route::group(['prefix' => 'plan'], fn() => [

    Route::get('/', [PlanController::class, 'index'])->middleware(['permission.check:plan-list'])->name('plan.index'),

    Route::get('/create', [PlanController::class, 'create'])->middleware(['permission.check:plan-create'])->name('plan.create'),

    Route::post('/create', [PlanController::class, 'store'])->middleware(['permission.check:plan-create'])->name('plan.store'),

    Route::get('/{plan}/edit', [PlanController::class, 'edit'])->middleware(['permission.check:plan-update'])->name('plan.edit'),

    Route::put('/{plan}/edit', [PlanController::class, 'update'])->middleware(['permission.check:plan-update'])->name('plan.update'),

    Route::get('/{plan}', [PlanController::class, 'show'])->middleware(['permission.check:plan-show'])->name('plan.show'),

    Route::get('destroy/{plan_id}', [PlanController::class, 'destroy'])->middleware(['permission.check:plan-destroy'])->name('plan.destroy')]),

    // Member Routes
    Route::group(['prefix' => 'member'], fn() => [

        Route::get('/', [MemberController::class, 'index'])->middleware(['permission.check:member-list'])->name('member.index'),

        Route::get('/create', [MemberController::class, 'create'])->middleware(['permission.check:member-create'])->name('member.create'),

        Route::post('/create', [MemberController::class, 'store'])->middleware(['permission.check:member-create'])->name('member.store'),

        Route::get('/{member}/edit', [MemberController::class, 'edit'])->middleware(['permission.check:member-update'])->name('member.edit'),

        Route::put('/{member}/edit', [MemberController::class, 'update'])->middleware(['permission.check:member-update'])->name('member.update'),

        Route::get('/{member}', [MemberController::class, 'show'])->middleware(['permission.check:member-show'])->name('member.show'),

        Route::get('destroy/{member_id}', [MemberController::class, 'destroy'])->middleware(['permission.check:member-destroy'])
            ->name('member.destroy')])
]);



