<?php

return [

    'super-admin' => [
        'partner-list' => 'super-admin-partner-list',
        'group-list' => 'super-admin-group-list',
        'region-list' => 'super-admin-region-list',
        'location-list' => 'super-admin-location-list',
        'user-management-list' => 'super-admin-user-management-list'

    ],
    'other' => [
        'partner-list' => 'user-management-partner-list',
        'group-list' => 'user-management-group-list',
        'region-list' => 'user-management-region-list',
        'location-list' => 'user-management-location-list'
    ]
];
